# Guestlist

Documentation v.1

## Installation
Create you python enviorement:
Create a folder outsite of this project to store you enviorement in fx.
```
py-env
```

Open the folder and create your python enviorement:
```bash
python3 -m venv guestlist
```

Activate your enviorement:
```python
source guestlist/bin/activate
```

Install requirements:
Download this repo and go to this project with your envioremnt activated. Run:
```python
cd guestlist_project
pip install -r requirements.txt
```

Run the project:
```python
python manage.py runserver
```